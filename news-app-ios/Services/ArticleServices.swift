//
//  ArticleServices.swift
//  news-app-ios
//
//  Created by Chien Pham on 7/28/17.
//  Copyright © 2017 Chien Pham. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

enum APIError: Error {
    case requestFailed(Error)
    case jsonParsingFailure(String)
    case jsonSerialization(String)
}

enum Result<T> {
    case success(T)
    case failure(APIError)
}

fileprivate let kJsonSerializationString = "json serialization failed"
fileprivate let kJsonParsingFailureString = "json parsing failed"

class ArticleService {
    
    private let appID = "c31b32"
    
    typealias CategoryCompletionHandler = (Result<[Category?]>) -> Void
    typealias ArticleListCompletionHandler = (Result<[Article?]>) -> Void
    typealias ArticleDetailCompletionHandler = (Result<Article?>) -> Void
    
    fileprivate let concurrentQueue = DispatchQueue(label: "article-service-queue", qos: .default, attributes: .concurrent)
    
    func fetchCategoryList(completionHandler: @escaping CategoryCompletionHandler) {
        let endpoint = "http://vnexpress-a.1api.vn/api/article?type=get_topstory&app_detail=1&app_id=\(appID)"
        
        Alamofire.request(endpoint, method: .get).validate(statusCode: 200..<300)
            .responseJSON(queue: concurrentQueue) { response in
                
                switch response.result {
                case .failure(let error):
                    
                    completionHandler(.failure(.requestFailed(error)))
                    
                case .success(let json):
                    
                    guard let json = json as? [String: Any] else {
                        completionHandler(.failure(.jsonSerialization(kJsonSerializationString)))
                        return
                    }
                    
                    guard let appDetailJson = json["app_detail"] as? [String: Any],
                        let configCategoryJson = appDetailJson["config_category"] as? [[String: Any]] else {
                            completionHandler(.failure(.jsonParsingFailure(kJsonParsingFailureString)))
                            return
                    }
                    
                    let categories = configCategoryJson.map { Category(JSON: $0) }
                    
                    DispatchQueue.main.async {
                        completionHandler(.success(categories))
                    }
                }
                
        }
        
    }
    
    func fetchTopArticleList(limit: Int = 30, completionHandler: @escaping ArticleListCompletionHandler) {
        let endpoint = "http://vnexpress-a.1api.vn/api/group?api[]=article%3Ftype%3Dget_topstory%26showed_area%3Dtrangchu_mobile%26option%3Dcate_parent%26cate_id%3D1000000%26limit%3D\(limit)&app_id=\(appID)&page=1"
        
        Alamofire.request(endpoint, method: .get).validate(statusCode: 200..<300)
            .responseJSON(queue: concurrentQueue) { response in
                
                switch response.result {
                case .failure(let error):
                    
                    completionHandler(.failure(.requestFailed(error)))
                    
                case .success(let json):
                    
                    guard let json = json as? [String: Any] else {
                        completionHandler(.failure(.jsonSerialization(kJsonSerializationString)))
                        return
                    }
                    
                    guard let data = json["data"] as? [String: Any] else {
                        completionHandler(.failure(.jsonParsingFailure(kJsonParsingFailureString)))
                        return
                    }
                    
                    guard let data0 = data["0"] as? [String: Any],
                        let homeData = data0["trangchu_mobile"] as? [[String: Any]] else {
                            completionHandler(.failure(.jsonParsingFailure(kJsonParsingFailureString)))
                            return
                    }
                    
                    let articles = homeData.map { Article(JSON: $0) }
                    
                    DispatchQueue.main.async {
                        completionHandler(.success(articles))
                    }
                    
                }
                
        }
        
    }
    
    func fetchArticleDetail(fromArticleId articleId: Int, completionHandler: @escaping ArticleDetailCompletionHandler) {
        
        let endpoint = "http://vnexpress-a.1api.vn/api/article?type=get_full&option=tag_detail,list_reference,cate_parent&app_id=\(appID)&article_id=\(articleId)&page=1"
        
        Alamofire.request(endpoint, method: .get).validate(statusCode: 200..<300)
            .responseJSON(queue: concurrentQueue) { response in
                
                switch response.result {
                case .failure(let error):
                    
                    completionHandler(.failure(.requestFailed(error)))
                    
                case .success(let json):
                    
                    guard let json = json as? [String: Any] else {
                        completionHandler(.failure(.jsonSerialization(kJsonSerializationString)))
                        return
                    }
                    
                    guard let data = json["data"] as? [String: Any] else {
                        completionHandler(.failure(.jsonParsingFailure(kJsonParsingFailureString)))
                        return
                    }
                    
                    let article = Article(JSON: data)
                    
                    DispatchQueue.main.async {
                        completionHandler(.success(article))
                    }
                    
                }
                
        }
        
    }
    
    func fetchArticleList(fromCategoryId categoryId: Int, offset: Int = 0, limit: Int = 30, completionHandler: @escaping ArticleListCompletionHandler) {
        let endpoint = "http://vnexpress-a.1api.vn/api/article?type=get_rule_2&option=cate_parent&app_id=\(appID)&cate_id=\(categoryId)&offset=\(offset)&limit=\(limit)"
        
        Alamofire.request(endpoint, method: .get).validate(statusCode: 200..<300)
            .responseJSON(queue: concurrentQueue) { response in
                
                switch response.result {
                case .failure(let error):
                    
                    completionHandler(.failure(.requestFailed(error)))
                    
                case .success(let json):
                    
                    guard let json = json as? [String: Any] else {
                        completionHandler(.failure(.jsonSerialization(kJsonSerializationString)))
                        return
                    }
                    
                    guard let data = json["data"] as? [String: Any],
                        let jsonForCategoryId = data["\(categoryId)"] as? [[String: Any]] else {
                            completionHandler(.failure(.jsonParsingFailure(kJsonParsingFailureString)))
                            return
                    }
                    
                    let articles = jsonForCategoryId.map { Article(JSON: $0) }
                    
                    DispatchQueue.main.async {
                        completionHandler(.success(articles))
                    }
                    
                }
                
        }
        
    }
}
