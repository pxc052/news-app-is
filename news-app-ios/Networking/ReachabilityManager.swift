//
//  ReachabilityManager.swift
//  news-app-ios
//
//  Created by Chien Pham on 7/30/17.
//  Copyright © 2017 Chien Pham. All rights reserved.
//

import UIKit
import ReachabilitySwift

protocol NetworkStatusListener: class {
    func networkStatusDidChange(isAvailable: Bool)
}

class ReachabilityManager: NSObject {
    
    static let shared = ReachabilityManager()
    
    private var listeners: [NetworkStatusListener] = []
    private var reachability: Reachability?
    private var currentReachabilityStatus: Reachability.NetworkStatus {
        guard let reachability = reachability else {
            return .notReachable
        }
        return reachability.currentReachabilityStatus
    }
    
    @objc private func reachabilityChanged(_ notification: Notification) {
        guard let reachability = notification.object as? Reachability else {
            return
        }
        switch reachability.currentReachabilityStatus {
        case .notReachable:
            debugPrint("Network became unreachable")
        case .reachableViaWiFi:
            debugPrint("Network reachable through WiFi")
        case .reachableViaWWAN:
            debugPrint("Network reacable through Cellular Data")
        }
        
        let flag = reachability.currentReachabilityStatus != .notReachable
        
        for listener in listeners {
            listener.networkStatusDidChange(isAvailable: flag)
        }
    }
    
    func addListener(_ listener: NetworkStatusListener) {
        listeners.append(listener)
    }
    
    func removeListener(_ listener: NetworkStatusListener) {
        listeners = listeners.filter { $0 !== listener }
    }
    
    func startMonitoring() {
        self.reachability = Reachability()!
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(_:)), name: ReachabilityChangedNotification, object: reachability)
        do {
            try reachability?.startNotifier()
        } catch let error {
            print("error \(error.localizedDescription)")
        }
    }
    
    func stopMonitoring() {
        reachability?.stopNotifier()
        NotificationCenter.default.removeObserver(self, name: ReachabilityChangedNotification, object: reachability)
    }
    
}
