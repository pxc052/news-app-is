//
//  Tag.swift
//  news-app-ios
//
//  Created by Chien Pham on 7/28/17.
//  Copyright © 2017 Chien Pham. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class Tag: Object, Mappable {
    
    dynamic var name: String = ""
    dynamic var id: Int = 0
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func mapping(map: Map) {
        id <- map["tag_id"]
        name <- map["tag_name"]
    }
    
}
