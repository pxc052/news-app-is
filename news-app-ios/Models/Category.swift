//
//  Category.swift
//  news-app-ios
//
//  Created by Chien Pham on 7/28/17.
//  Copyright © 2017 Chien Pham. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class Category: Object, Mappable {
    dynamic var id: Int = 0
    dynamic var code: String = ""
    dynamic var name: String = ""
    dynamic var displayOrder: Int = 0
    dynamic var showFolder: Int = 0
    dynamic var level: Int = 0
    dynamic var fullParent: String = ""
    dynamic var parentId: Int = 0
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func mapping(map: Map) {
        id <- map["category_id"]
        code <- map["catecode"]
        name <- map["catename"]
        displayOrder <- map["display_order"]
        level <- map["level"]
        showFolder <- map["show_folder"]
        fullParent <- map["full_parent"]
        parentId <- map["parent_id"]
    }
    
    static func add(_ categories: [Category]) {
        if categories.count == 0 {
            return
        }
        let realm = try! Realm()
        try! realm.write {
            realm.add(categories)
        }
    }
    
    static func getAll() -> Results<Category> {
        let realm = try! Realm()
        return realm.objects(Category.self)
    }
    
}









