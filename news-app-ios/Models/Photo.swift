//
//  Photo.swift
//  news-app-ios
//
//  Created by Chien Pham on 7/28/17.
//  Copyright © 2017 Chien Pham. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class Photo: Object, Mappable {
    
    dynamic var id: Int = 0
    dynamic var imageUrl: String = ""
    dynamic var caption: String = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func mapping(map: Map) {
        id <- map["photo_id"]
        imageUrl <- map["thumbnail_url"]
        caption <- map["caption"]
    }
    
}
