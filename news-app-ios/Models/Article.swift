//
//  Article.swift
//  news-app-ios
//
//  Created by Chien Pham on 7/28/17.
//  Copyright © 2017 Chien Pham. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class Article: Object, Mappable {
    dynamic var id: Int = 0
    dynamic var title: String = ""
    dynamic var lead: String = ""
    dynamic var thumbnailUrl: String = ""
    dynamic var publishTime: Int = 0
    dynamic var updateTime: Int = 0
    dynamic var totalComment: Int = 0
    dynamic var content: String? = ""
    dynamic var parentCategory: Category?
    dynamic var isFavorite = false
    dynamic var isRead = false
    var photos = List<Photo>()
    var tags = List<Tag>()
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func mapping(map: Map) {
        id <- map["article_id"]
        title <- map["title"]
        lead <- map["lead"]
        thumbnailUrl <- map["thumbnail_url"]
        publishTime <- map["publish_time"]
        updateTime <- map["update_time"]
        totalComment <- map["total_comment"]
        content <- map["content"]
        parentCategory <- map["cate_parent"]
        photos <- (map["photos"], RealmListTransform<Photo>())
        tags <- (map["tag_detail"], RealmListTransform<Tag>())
    }
    
    static func add(_ articles: [Article]) {
        if articles.count == 0 {
            return
        }
        let realm = try! Realm()
        try! realm.write {
            realm.add(articles, update: true)
        }
    }
    
    static func finding(_ id: Int?) -> Article? {
        guard let id = id else { return nil }
        let realm = try! Realm()
        return realm.object(ofType: Article.self, forPrimaryKey: id)
    }
    
    static func getFavoriteList() -> Results<Article> {
        let realm = try! Realm()
        return realm.objects(Article.self).filter("isFavorite == true")
    }
    
    static func removeAllFromFavoriteList() {
        let articles = getFavoriteList()
        for article in articles {
            article.removeFromFavoriteList()
        }
    }
    
    static func getHistoryList() -> Results<Article> {
        let realm = try! Realm()
        return realm.objects(Article.self).filter("isRead == true")
    }
    
    static func removeAllFromHistoryList() {
        let articles = getHistoryList()
        for article in articles {
            article.removeFromHistoryList()
        }
    }
    
}

extension Article {
    
    func addToFavoriteList() {
        let realm = try! Realm()
        try! realm.write {
            self.isFavorite = true
            realm.create(Article.self, value: self, update: true)
        }
    }
    
    func removeFromFavoriteList() {
        let realm = try! Realm()
        let dbArticle = Article.finding(self.id)
        if dbArticle != nil {
            try! realm.write {
                dbArticle!.isFavorite = false
            }
        }
    }
    
    func addToHistoryList() {
        let realm = try! Realm()
        try! realm.write {
            self.isRead = true
            realm.create(Article.self, value: self, update: true)
        }
    }
    
    func removeFromHistoryList() {
        let realm = try! Realm()
        let dbArticle = Article.finding(self.id)
        if dbArticle != nil {
            try! realm.write {
                dbArticle!.isRead = false
            }
        }
    }
    
}





