//
//  NSObject+Extension.swift
//  news-app-ios
//
//  Created by Chien Pham on 7/28/17.
//  Copyright © 2017 Chien Pham. All rights reserved.
//

import UIKit

extension NSObject {
    
    public var className: String {
        return type(of: self).className
    }
    
    public static var className: String {
        return String.className(aClass: self)
    }
    
}
