//
//  UITableView+Extension.swift
//  news-app-ios
//
//  Created by Chien Pham on 8/6/17.
//  Copyright © 2017 Chien Pham. All rights reserved.
//

import UIKit

extension UITableView {
    
    func registerCellNib(forCell cellClass: AnyClass) {
        let identifier = String.className(aClass: cellClass)
        let nib = UINib(nibName: identifier, bundle: nil)
        register(nib, forCellReuseIdentifier: identifier)
    }
    
    func dequeue<T>(cellClass: T.Type, forIndexPath indexPath: IndexPath) -> T where T: UITableViewCell {
        guard let cell = dequeueReusableCell(withIdentifier: cellClass.className, for: indexPath) as? T else {
            fatalError("Could not dequeue cell with identifier: \(cellClass.className)")
        }
        return cell
    }
    
}

