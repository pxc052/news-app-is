//
//  NSAttributedString+Extension.swift
//  news-app-ios
//
//  Created by Chien Pham on 7/28/17.
//  Copyright © 2017 Chien Pham. All rights reserved.
//

import UIKit

extension NSAttributedString {
    
    func setFamilyFont(name fontName: String, size: CGFloat) -> NSAttributedString? {
        
        let newAttributedString = NSMutableAttributedString(attributedString: self)
        
        newAttributedString.enumerateAttribute(NSFontAttributeName, in: NSMakeRange(0, newAttributedString.length), options: []) { value, range, stop in
            guard let currentFont = value as? UIFont else {
                return
            }
            
            let fontDescriptor = currentFont.fontDescriptor.addingAttributes([UIFontDescriptorFamilyAttribute: fontName])
            
            if let newFontDescriptor = fontDescriptor.matchingFontDescriptors(withMandatoryKeys: [UIFontDescriptorFamilyAttribute]).first {
                let newFont = UIFont(descriptor: newFontDescriptor, size: size)
                newAttributedString.addAttributes([NSFontAttributeName: newFont], range: range)
            }
            
        }
        
        return newAttributedString
        
    }
    
    func resizeImages(aligment: NSTextAlignment) -> NSAttributedString? {
        
        let newAttributedString = NSMutableAttributedString(attributedString: self)
        
        newAttributedString.enumerateAttribute(NSAttachmentAttributeName, in: NSMakeRange(0, newAttributedString.length), options: []) { value, range, stop in
            
            guard let currentAttachment = value as? NSTextAttachment else {
                return
            }
            
            let screenSize = UIScreen.main.bounds
            
            let maxWidth = screenSize.width - 16
            
            let bounds = currentAttachment.bounds
            
            var newSize = bounds
            
            if bounds.width > bounds.height {
                if bounds.width > maxWidth {
                    let ratio = maxWidth / bounds.width
                    let newHeight = bounds.height * ratio
                    newSize = CGRect(x: 0, y: 0, width: maxWidth, height: newHeight)
                }
            } else {
                let newWidth = bounds.width * 0.5
                let newHeight = bounds.height * 0.5
                newSize = CGRect(x: 0, y: 0, width: newWidth, height: newHeight)
            }
            
            currentAttachment.bounds = newSize
            
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.alignment = aligment
            newAttributedString.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: range)
            
        }
        
        return newAttributedString
        
    }
    
}
