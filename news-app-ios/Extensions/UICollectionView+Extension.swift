//
//  UICollectionView+Extension.swift
//  news-app-ios
//
//  Created by Chien Pham on 7/28/17.
//  Copyright © 2017 Chien Pham. All rights reserved.
//

import UIKit

extension UICollectionView {
    
    func registerCellNib(forCell cellClass: AnyClass) {
        let identifier = String.className(aClass: cellClass)
        let nib = UINib(nibName: identifier, bundle: nil)
        register(nib, forCellWithReuseIdentifier: identifier)
    }
    
    func dequeue<T>(cellClass: T.Type, forIndexPath indexPath: IndexPath) -> T where T: UICollectionViewCell {
        guard let cell = dequeueReusableCell(withReuseIdentifier: cellClass.className, for: indexPath) as? T else {
            fatalError("Could not dequeue cell with identifier: \(cellClass.className)")
        }
        return cell
    }

}
