//
//  String+Extension.swift
//  news-app-ios
//
//  Created by Chien Pham on 7/28/17.
//  Copyright © 2017 Chien Pham. All rights reserved.
//

import UIKit

extension String {
    
    static func className(aClass: AnyClass) -> String {
        return String(describing: aClass)
    }
    
}
