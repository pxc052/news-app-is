//
//  ImageViewController.swift
//  news-app-ios
//
//  Created by Chien Pham on 8/5/17.
//  Copyright © 2017 Chien Pham. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    @IBAction func cancel(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }

}
