//
//  MenuCell.swift
//  news-app-ios
//
//  Created by Chien Pham on 8/5/17.
//  Copyright © 2017 Chien Pham. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = UIColor.clear
        contentView.backgroundColor = UIColor.clear
        titleLabel.textColor = UIColor.white
    }
    
}
