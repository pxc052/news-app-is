//
//  SideMenuViewController.swift
//  news-app-ios
//
//  Created by Chien Pham on 8/5/17.
//  Copyright © 2017 Chien Pham. All rights reserved.
//

import UIKit

class SideMenuViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.separatorStyle = .none
        self.tableView.backgroundColor = UIColor.clear
        self.tableView.registerCellNib(forCell: MenuCell.self)
        self.tableView.registerCellNib(forCell: UserCell.self)
    }
    
}

extension SideMenuViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeue(cellClass: UserCell.self, forIndexPath: indexPath)
            return cell
        case 1:
            let cell = tableView.dequeue(cellClass: MenuCell.self, forIndexPath: indexPath)
            cell.iconImageView.image = UIImage(named: "history")
            cell.titleLabel?.text = "Tin đã đọc"
            return cell
        case 2:
            let cell = tableView.dequeue(cellClass: MenuCell.self, forIndexPath: indexPath)
            cell.iconImageView.image = UIImage(named: "star")
            cell.titleLabel?.text = "Tin đánh dấu"
            return cell
        case 3:
            let cell = tableView.dequeue(cellClass: MenuCell.self, forIndexPath: indexPath)
            cell.iconImageView.image = UIImage(named: "gold")
            cell.titleLabel?.text = "Giá vàng"
            return cell
        case 4:
            let cell = tableView.dequeue(cellClass: MenuCell.self, forIndexPath: indexPath)
            cell.iconImageView.image = UIImage(named: "usd")
            cell.titleLabel?.text = "Giá ngoại tệ"
            return cell
        case 5:
            let cell = tableView.dequeue(cellClass: MenuCell.self, forIndexPath: indexPath)
            cell.iconImageView.image = UIImage(named: "weather")
            cell.titleLabel?.text = "Thời tiết"
            return cell
        case 6:
            let cell = tableView.dequeue(cellClass: MenuCell.self, forIndexPath: indexPath)
            cell.iconImageView.image = UIImage(named: "paper_plane")
            cell.titleLabel?.text = "Góp ý"
            return cell
        case 7:
            let cell = tableView.dequeue(cellClass: MenuCell.self, forIndexPath: indexPath)
            cell.iconImageView.image = UIImage(named: "settings")
            cell.titleLabel?.text = "Cài đặt"
            return cell
        default:
            return UITableViewCell()
        }

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return CGFloat(CellHeigh.UserCell)
        default:
            return CGFloat(CellHeigh.MenuCell)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        sideMenuController?.hideLeftView()
        
        guard let nav = sideMenuController?.rootViewController as? NavigationController else {
            return
        }
        
        var destinationVC: UIViewController?
        
        switch indexPath.row {
        case 0:
            destinationVC = HistoryViewController(nibName: "HistoryViewController", bundle: nil)
        case 1:
            destinationVC = HistoryViewController(nibName: "HistoryViewController", bundle: nil)
        case 2:
            destinationVC = FavoriteViewController(nibName: "FavoriteViewController", bundle: nil)
        case 3:
            destinationVC = HistoryViewController(nibName: "HistoryViewController", bundle: nil)
        case 4:
            destinationVC = HistoryViewController(nibName: "HistoryViewController", bundle: nil)
        case 5:
            destinationVC = HistoryViewController(nibName: "HistoryViewController", bundle: nil)
        case 6:
            // TODO
            break
        case 7:
            break
        default:
            break
        }
        
        guard let unwrappedDestinationVC = destinationVC else { return }
        
        nav.pushViewController(unwrappedDestinationVC, animated: true)
        
    }
    
}
