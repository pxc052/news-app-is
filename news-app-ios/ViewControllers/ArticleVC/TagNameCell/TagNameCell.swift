//
//  TagNameCell.swift
//  news-app-ios
//
//  Created by Chien Pham on 7/29/17.
//  Copyright © 2017 Chien Pham. All rights reserved.
//

import UIKit

class TagNameCell: UICollectionViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var roundedView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.roundedView.layer.cornerRadius = 15
        self.roundedView.clipsToBounds = true
        self.nameLabel.textColor = UIColor.white
    }

}
