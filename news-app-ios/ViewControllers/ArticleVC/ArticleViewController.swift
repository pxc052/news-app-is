//
//  ArticleViewController.swift
//  news-app-ios
//
//  Created by Chien Pham on 7/28/17.
//  Copyright © 2017 Chien Pham. All rights reserved.
//

import UIKit

class ArticleViewController: BaseViewController {
    
    // MARK: - Properties
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    private let articleService = ArticleService()
    private let heartFilled = UIImage(named: "heart_filled")?.withRenderingMode(.alwaysOriginal)
    private let heartNormal = UIImage(named: "heart_normal")?.withRenderingMode(.alwaysOriginal)
    
    fileprivate lazy var article: Article? = { Article.finding(self.articleID) }()
    
    var articleID: Int?
    
    // MARK: - Functions
    
    override func setup() {
        self.collectionView.registerCellNib(forCell: ArticleDetailCell.self)
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.showsVerticalScrollIndicator = false
        self.collectionView.showsHorizontalScrollIndicator = false
        self.collectionView.bounces = false
        
        
        if article != nil {
            addFavoriteButton(isFavorite: article!.isFavorite)
            self.title = article!.parentCategory?.name
        } else {
            fetchArticle(from: self.articleID)
            addFavoriteButton(isFavorite: false)
        }
        
    }
    
    private func addFavoriteButton(isFavorite: Bool = false) {
        let image = isFavorite ? heartFilled : heartNormal
        let button = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(didTapFavoriteButton(_:)))
        self.navigationItem.rightBarButtonItem = button
    }
    
    func didTapFavoriteButton(_ sender: UIBarButtonItem) {
        
        guard let article = self.article else { return }
        
        // find current favorite state
        
        let image = sender.image
        let isFavorite = image == heartNormal
        
        sender.image = isFavorite ? heartFilled : heartNormal
        
        if isFavorite {
            article.addToFavoriteList()
        } else {
            article.removeFromFavoriteList()
        }
        
    }
    
    private func fetchArticle(from id: Int?) {
        guard let id = id else { return }
        self.articleService.fetchArticleDetail(fromArticleId: id) { [weak self] result in
            guard let strongSelf = self else { return }
            switch result {
            case .failure(_):
                // TODO: Handle error
                break
            case .success(let article):
                strongSelf.article = article
                strongSelf.article?.addToHistoryList()
                strongSelf.title = article?.parentCategory?.name
                strongSelf.collectionView.reloadData()
            }
        }
    }
    
}

extension ArticleViewController: UICollectionViewDataSource {
    
    // MARK: - UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeue(cellClass: ArticleDetailCell.self, forIndexPath: indexPath)
        cell.configure(from: self.article)
        return cell
    }
    
}

extension ArticleViewController: UICollectionViewDelegateFlowLayout {
    
    // MARK: - UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        // dynamic cell height
        
        let insets = collectionView.contentInset.left + collectionView.contentInset.right
        let targetWidth = collectionView.frame.width - insets
        
        let nibName = String.className(aClass: ArticleDetailCell.self)
        let cell = Bundle.main.loadNibNamed(nibName, owner: self, options: nil)?[0] as? ArticleDetailCell
        
        cell!.configure(from: self.article)
        cell!.bounds = CGRect(x: 0, y: 0, width: targetWidth, height: cell!.bounds.height)
        cell!.contentView.bounds = cell!.bounds
        cell!.setNeedsLayout()
        cell!.layoutIfNeeded()
        
        var size = cell!.systemLayoutSizeFitting(UILayoutFittingCompressedSize)
        size.width = targetWidth
        
        return size
    }
    
}





















