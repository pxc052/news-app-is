//
//  ArticleDetailCell.swift
//  news-app-ios
//
//  Created by Chien Pham on 7/29/17.
//  Copyright © 2017 Chien Pham. All rights reserved.
//

import UIKit

class ArticleDetailCell: ArticleCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var categoryNameLabel: UILabel!
    @IBOutlet weak var publishTimeLabel: UILabel!
    @IBOutlet weak var contentTextField: UITextView!
    @IBOutlet weak var tagImageView: UIImageView!
    @IBOutlet weak var clockImageView: UIImageView!
    @IBOutlet weak var tagCollectionView: UICollectionView!
    @IBOutlet weak var subtitleView: UIView!
    
    private let clockNormalImage = UIImage(named: "clock_normal")
    private let tagFilledImage = UIImage(named: "tag_filled")
    
    fileprivate var offScreenCells: [Int: TagNameCell] = [:]
    
    var article: Article?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setupUI() {
        
        // setup initial state
        subtitleView.backgroundColor = UIColor.white
        titleLabel.text = ""
        subtitleLabel.text = ""
        publishTimeLabel.text = ""
        categoryNameLabel.text = ""
        tagImageView.image = nil
        clockImageView.image = nil
        contentTextField.text = ""
        contentTextField.attributedText = nil
        tagCollectionView.dataSource = self
        tagCollectionView.delegate = self
        tagCollectionView.registerCellNib(forCell: TagNameCell.self)
        
    }
    
    override func configure(from article: Article?) {
        guard let article = article else { return }
        self.article = article
        
        subtitleView.backgroundColor = UIColor(red: 235/255, green: 235/255, blue: 241/255, alpha: 1)
        setTitleText(article.title)
        setSubtitleText(article.lead)
        setPublishTime(article.publishTime)
        setClockImage()
        setContent(article.content)
        tagImageView.image = tagFilledImage
        categoryNameLabel.text = article.parentCategory?.name
        tagCollectionView.reloadData()
    }

}

extension ArticleDetailCell: UICollectionViewDataSource {
    
    // MARK: - UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return article?.tags.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeue(cellClass: TagNameCell.self, forIndexPath: indexPath)
        
        cell.nameLabel.text = article?.tags[indexPath.row].name
        
        return cell
    }
    
}

extension ArticleDetailCell: UICollectionViewDelegateFlowLayout {
    
    // MARK: - UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        // dynamic cell width
        
        var cell = offScreenCells[indexPath.row]
        let targetHeight: CGFloat = tagCollectionView.frame.height
        
        if cell == nil {
            let nibName = String.className(aClass: TagNameCell.self)
            cell = Bundle.main.loadNibNamed(nibName, owner: self, options: nil)?[0] as? TagNameCell
            offScreenCells[indexPath.row] = cell
        }
        
        cell!.nameLabel.text = article?.tags[indexPath.row].name
        cell!.setNeedsLayout()
        cell!.layoutIfNeeded()
        
        var size = cell!.systemLayoutSizeFitting(UILayoutFittingCompressedSize)
        size.height = targetHeight
        
        return size
    }
    
}

extension ArticleDetailCell: ArticleWithTitle, ArticleWithSubtitle, ArticleWithPublishTime, ArticleWithContent {}



