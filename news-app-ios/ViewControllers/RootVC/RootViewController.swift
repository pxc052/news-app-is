//
//  RootViewController.swift
//  news-app-ios
//
//  Created by Chien Pham on 7/28/17.
//  Copyright © 2017 Chien Pham. All rights reserved.
//

import UIKit
import PageMenuKit
import RealmSwift

class RootViewController: BaseViewController {
    
    // MARK: - Properties
    
    private let articleService = ArticleService()
    
    private var pageMenuController: PMKPageMenuController?
    private var viewControllers: [ArticleListViewController] = []
    
    private lazy var categoryList: Results<Category> = { return Category.getAll() }()
    
    // MARK: - Functions
    
    override func setup() {
        self.title = "Tin Tức"
        // custom navigation bar back button image and title
        setDefaultBackButton()
        addBarButtons()
        /*  
                BUG: menu disappear after pop a view controller
            when pop a viewcontroller from stack
            the contentInset of the scrollview will inscrease from 0 to 64
            Currently, i fix this problem by putting this code
        */
        self.automaticallyAdjustsScrollViewInsets = false
        
        if categoryList.isEmpty {
            fetchCategoryList()
        } else {
            addPageMenuController()
        }
        
    }
    
    fileprivate func fetchCategoryList() {
        self.articleService.fetchCategoryList { [weak self] result in
            guard let strongSelf = self else { return }
            switch result {
            case .failure(_):
                // TODO: Handle error
                break
            case .success(let categories):
                var categoryList: [Category] = []
                
                let category = Category()
                category.name = "Tiêu điểm"
                category.id = 1000000
                categoryList.insert(category, at: 0)
                
                categoryList.append(category)
                
                for cate in categories {
                    
                    if let cate = cate {
                        
                        switch cate.name {
                            case "Video", "Tâm sự", "Cười":
                            break
                        default:
                            categoryList.append(cate)
                        }
                        
                    }
                    
                }
                // save to realm db
                Category.add(categoryList)
                
                strongSelf.addPageMenuController()
            }
        }
    }
    
    private func addPageMenuController() {
        
        for category in self.categoryList {
            let viewController = ArticleListViewController(nibName: "ArticleListViewController", bundle: nil)
            viewController.title = category.name
            viewController.category = category
            self.viewControllers.append(viewController)
        }
        
        let statusBarHeight = UIApplication.shared.statusBarFrame.size.height
        let navBarHeight = navigationController?.navigationBar.bounds.size.height ?? 0
        let topBarHeight = statusBarHeight + navBarHeight
        
        self.pageMenuController = PMKPageMenuController(controllers: viewControllers, menuStyle: .NHK, menuColors: [], topBarHeight: topBarHeight)
        addChildViewController(self.pageMenuController!)
        view.addSubview(self.pageMenuController!.view!)
        
        self.pageMenuController!.didMove(toParentViewController: self)
        self.pageMenuController!.view!.translatesAutoresizingMaskIntoConstraints = false
        
        let pageView = pageMenuController!.view!
        
        NSLayoutConstraint.activate([
            pageView.topAnchor.constraint(equalTo: view.topAnchor),
            pageView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            pageView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            pageView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
            ])
        
    }
    
    private func addBarButtons() {
        // disable default tint color by using original mode
        let searchButton = UIBarButtonItem(image: UIImage(named: "search")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(searchButtonTapped(_:)))
        let menuButton = UIBarButtonItem(image: UIImage(named: "menu")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(menuButtonTapped(_:)))
        navigationItem.leftBarButtonItem = menuButton
        navigationItem.rightBarButtonItem = searchButton
    }
    
    @objc private func searchButtonTapped(_ sender: UIBarButtonItem) {
        
    }
    
    @objc private func menuButtonTapped(_ sender: UIBarButtonItem) {
        sideMenuController?.showLeftView(animated: true, completionHandler: nil)
    }
    
}



























