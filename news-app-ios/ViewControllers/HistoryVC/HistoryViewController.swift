//
//  HistoryViewController.swift
//  news-app-ios
//
//  Created by Chien Pham on 8/6/17.
//  Copyright © 2017 Chien Pham. All rights reserved.
//

import UIKit
import RealmSwift

class HistoryViewController: BaseViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    fileprivate var articleList: Results<Article>!
    
    override func viewWillAppear(_ animated: Bool) {
        articleList = Article.getHistoryList()
        collectionView.reloadData()
    }
    
    override func setup() {
        
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.registerCellNib(forCell: SmallImageCell.self)
        self.collectionView.showsVerticalScrollIndicator = false
        self.collectionView.showsHorizontalScrollIndicator = false
        
        setDefaultBackButton()
        addClearAllButton()
        
    }
    
    private func addClearAllButton() {
        let clearAll = UIBarButtonItem(title: "Xoá hết", style: .plain, target: self, action: #selector(clearAllButtonTapped(_:)))
        clearAll.tintColor = UIColor.black
        navigationItem.rightBarButtonItem = clearAll
    }
    
    func clearAllButtonTapped(_ sender: UIBarButtonItem) {
        Article.removeAllFromHistoryList()
        self.collectionView.reloadData()
    }
    
}


extension HistoryViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.articleList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeue(cellClass: SmallImageCell.self, forIndexPath: indexPath)
        let article = self.articleList[indexPath.row]
        cell.configure(from: article)
        return cell
        
        
    }
}

extension HistoryViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        guard let cell = cell as? ArticleWithImageView else { return }
        
        // fade in image view
        
        cell.thumbnailImageView.alpha = 0.0
        UIView.animate(withDuration: 0.5) {
            cell.thumbnailImageView.alpha = 1.0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        guard let cell = cell as? ArticleWithImageView else { return }
        
        // fade out image view
        
        cell.thumbnailImageView.alpha = 1.0
        UIView.animate(withDuration: 0.5) {
            cell.thumbnailImageView.alpha = 0.0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        // Push article view controller to stack
        
        let articleVC = ArticleViewController(nibName: "ArticleViewController", bundle: nil)
        let article = self.articleList[indexPath.row]
        articleVC.articleID = article.id
        self.navigationController?.pushViewController(articleVC, animated: true)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let insets = collectionView.contentInset.left + collectionView.contentInset.right
        
        return CGSize(width: collectionView.frame.width - insets, height: CGFloat(CellHeigh.Default))
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 4.0
    }
    
}
