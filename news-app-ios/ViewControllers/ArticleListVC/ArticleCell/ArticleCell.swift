//
//  ArticleCollectionViewCell.swift
//  news-app-ios
//
//  Created by Chien Pham on 7/28/17.
//  Copyright © 2017 Chien Pham. All rights reserved.
//

import UIKit
import Kingfisher
import SwiftSoup

class ArticleCell: UICollectionViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    func setupUI() {}
    func configure(from article: Article?) {}
    
}








