//
//  SmallImageCell.swift
//  news-app-ios
//
//  Created by Chien Pham on 8/3/17.
//  Copyright © 2017 Chien Pham. All rights reserved.
//

import UIKit

class SmallImageCell: ArticleCell {
    
    @IBOutlet weak var publishTimeLabel: UILabel!
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var clockImageView: UIImageView!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    var placeHolderImage: UIImage? = UIImage(named: "")
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    override func setupUI() {
        // setup initial state
        self.subtitleLabel.text = ""
        self.publishTimeLabel.text = ""
        self.thumbnailImageView.image = nil
        self.clockImageView.image = nil
    }
    
    override func configure(from article: Article?) {
        guard let article = article else { return }
        setSubtitleText(article.lead)
        setThumbnailImage(with: article.thumbnailUrl)
        setPublishTime(article.publishTime)
        setClockImage()
    }
    
}

extension SmallImageCell: ArticleWithImageView, ArticleWithSubtitle, ArticleWithPublishTime {}













