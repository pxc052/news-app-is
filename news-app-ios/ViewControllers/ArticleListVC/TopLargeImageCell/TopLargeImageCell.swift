//
//  TopLargeImageCell.swift
//  news-app-ios
//
//  Created by Chien Pham on 8/2/17.
//  Copyright © 2017 Chien Pham. All rights reserved.
//

import UIKit

class TopLargeImageCell: ArticleCell {

    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var publishTimeLabel: UILabel!
    @IBOutlet weak var clockImageView: UIImageView!
    var placeHolderImage: UIImage? = UIImage(named: "")
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setupUI() {
        // setup initial state
        self.titleLabel.text = ""
        self.subtitleLabel.text = ""
        self.publishTimeLabel.text = ""
        self.thumbnailImageView.image = nil
        self.clockImageView.image = nil
    }
    
    override func configure(from article: Article?) {
        guard let article = article else { return }
        setTitleText(article.title)
        setSubtitleText(article.lead)
        setThumbnailImage(with: article.thumbnailUrl)
        setPublishTime(article.publishTime)
        setClockImage()
    }

}

extension TopLargeImageCell: ArticleWithImageView, ArticleWithTitle, ArticleWithSubtitle, ArticleWithPublishTime {}
