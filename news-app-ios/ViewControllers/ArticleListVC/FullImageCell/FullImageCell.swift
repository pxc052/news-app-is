//
//  FullImageCell.swift
//  news-app-ios
//
//  Created by Chien Pham on 8/2/17.
//  Copyright © 2017 Chien Pham. All rights reserved.
//

import UIKit

class FullImageCell: ArticleCell {

    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var placeHolderImage: UIImage? = UIImage(named: "")
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setupUI() {
        // setup initial state
        self.titleLabel.text = ""
        self.thumbnailImageView.image = nil
    }
    
    override func configure(from article: Article?) {
        guard let article = article else { return }
        setTitleText(article.title)
        setThumbnailImage(with: article.thumbnailUrl)
    }
    
}


extension FullImageCell: ArticleWithImageView, ArticleWithTitle {}
