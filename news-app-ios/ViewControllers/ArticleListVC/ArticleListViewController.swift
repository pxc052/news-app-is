//
//  ArticleListViewController.swift
//  news-app-ios
//
//  Created by Chien Pham on 7/28/17.
//  Copyright © 2017 Chien Pham. All rights reserved.
//

import UIKit
import PullToRefreshKit

class ArticleListViewController: BaseViewController {
    
    // MARK: - Properties
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    private let articleService = ArticleService()
    
    private var isLoading = false
    
    fileprivate var articleList: [Article] = []
    
    var category: Category?
    
    // MARK: - Functions
    
    override func setup() {
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.registerCellNib(forCell: SmallImageCell.self)
        self.collectionView.showsVerticalScrollIndicator = false
        self.collectionView.showsHorizontalScrollIndicator = false
        
        if self.category?.id != 1000000 {
            setupPullToRefresh()
            setupLoadMore()
        }
        
        fetchArticleList(from: category?.id)
        
    }
    
    private func setupPullToRefresh() {
        _ = collectionView.setUpHeaderRefresh { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.refresh()
            strongSelf.collectionView.endHeaderRefreshing()
        }
    }
    
    private func setupLoadMore() {
        _ = collectionView.setUpFooterRefresh { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.insertNewArticles()
            strongSelf.collectionView.endFooterRefreshing()
        }
    }
    
    fileprivate func fetchArticleList(from id: Int?, offset: Int = 0, limit: Int = 30) {
        /*
         if the user keep pulling down to load more data, we just abort the incoming tasks
         until the current task is completed.
         */
        if !self.isLoading { // check if the current task is running
            self.isLoading = true
            guard let id = id else { return }
            if id == 1000000 {
                // get articles for home page
                self.articleService.fetchTopArticleList(limit: limit) { [weak self] result in
                    guard let strongSelf = self else { return }
                    switch result {
                    case .failure(_):
                        // TODO: Handle error
                        break
                    case .success(let articles):
                        strongSelf.insertCells(for: articles, currentOffset: offset)
                        strongSelf.isLoading = false
                    }
                }
            } else {
                // get articles with category name
                self.articleService.fetchArticleList(fromCategoryId: id, offset: offset, limit: limit) { [weak self] result in
                    guard let strongSelf = self else { return }
                    switch result {
                    case .failure(_):
                        // TODO: Handle error
                        break
                    case .success(let articles):
                        strongSelf.insertCells(for: articles, currentOffset: offset)
                        strongSelf.isLoading = false
                    }
                }
            }
        }
    }
    
    private func refresh() {
        deleteAticles()
        if self.category?.id == 1000000 {
            self.fetchArticleList(from: self.category?.id)
        } else {
            insertNewArticles()
        }
    }
    
    private func deleteAticles() {
        self.collectionView.performBatchUpdates({
            self.articleList.removeAll()
            self.collectionView.reloadData()
        }, completion: nil)
    }
    
    private func insertNewArticles() {
        if self.category?.id == 1000000 {
            // api does not support offset parameter
            return
        }
        // get more articles from current offset
            let currentOffset = self.articleList.count
            self.fetchArticleList(from: self.category?.id, offset: currentOffset)
    }
    
    private func insertCells(for articles: [Article?], currentOffset: Int) {
        self.articleList.append(contentsOf: articles as! [Article])
        if articles.count == 0 {
            return
        }
        if currentOffset == 0 {
            self.collectionView.reloadData()
        } else {
            
            // insert cells for new fetched articles
            
            self.collectionView.performBatchUpdates({
                var newIndexPaths: [IndexPath] = []
                for index in currentOffset..<self.articleList.count {
                    let indexPath = IndexPath(row: index, section: 0)
                    newIndexPaths.append(indexPath)
                }
                self.collectionView.insertItems(at: newIndexPaths)
            }, completion: nil)
        }
    }
    
}

extension ArticleListViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.articleList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeue(cellClass: SmallImageCell.self, forIndexPath: indexPath)
        let article = self.articleList[indexPath.row]
        cell.configure(from: article)
        return cell
        
    }
}

extension ArticleListViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        guard let cell = cell as? ArticleWithImageView else { return }
        
        // fade in image view
        
        cell.thumbnailImageView.alpha = 0.0
        UIView.animate(withDuration: 0.5) {
            cell.thumbnailImageView.alpha = 1.0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        guard let cell = cell as? ArticleWithImageView else { return }
        
        // fade out image view
        
        cell.thumbnailImageView.alpha = 1.0
        UIView.animate(withDuration: 0.5) {
            cell.thumbnailImageView.alpha = 0.0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        // Push article view controller to stack
        let article = self.articleList[indexPath.row]
        let articleVC = ArticleViewController(nibName: "ArticleViewController", bundle: nil)
        articleVC.articleID = article.id
        self.navigationController?.pushViewController(articleVC, animated: true)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let insets = collectionView.contentInset.left + collectionView.contentInset.right
        
        return CGSize(width: collectionView.frame.width - insets, height: CGFloat(CellHeigh.Default))
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 4.0
    }
    
}
































