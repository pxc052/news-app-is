//
//  BaseCollectionViewCell.swift
//  news-app-ios
//
//  Created by Chien Pham on 7/28/17.
//  Copyright © 2017 Chien Pham. All rights reserved.
//

import UIKit

class BaseCollectionViewCell: UICollectionViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    func setupUI() {}
    
}
