//
//  ArticleProtocol.swift
//  news-app-ios
//
//  Created by Chien Pham on 8/6/17.
//  Copyright © 2017 Chien Pham. All rights reserved.
//

import UIKit
import Kingfisher
import SwiftSoup

protocol ArticleWithImageView {
    
    var placeHolderImage: UIImage? { get set }
    var thumbnailImageView: UIImageView! { get set }
    
    func setThumbnailImage(with urlString: String?)
    
}

extension ArticleWithImageView {
    
    func setThumbnailImage(with urlString: String?) {
        guard let urlString = urlString,
            let url = URL(string: urlString) else { return }
        thumbnailImageView.kf.setImage(with: url, placeholder: placeHolderImage, options: [.transition(.fade(0.5))])
        
    }
    
}

protocol ArticleWithTitle {
    
    var titleLabel: UILabel! { get set }
    func setTitleText(_ text: String?)
    
}

extension ArticleWithTitle {
    
    func setTitleText(_ text: String?) {
        titleLabel.text = text
    }
    
}

protocol ArticleWithSubtitle {
    
    var subtitleLabel: UILabel! { get set }
    func setSubtitleText(_ text: String?)
    
}

extension ArticleWithSubtitle {
    
    func setSubtitleText(_ text: String?) {
        subtitleLabel.text = text
    }
    
}

protocol ArticleWithPublishTime {
    
    var publishTimeLabel: UILabel! { get set }
    var clockImageView: UIImageView! { get set }
    
    func setPublishTime(_ time: Int)
    func setClockImage()
}

extension ArticleWithPublishTime {
    
    private func publishTime(from unixTime: Int) -> String {
        
        let date = Date()
        let calendar = Calendar.current
        
        let date2 = Date(timeIntervalSince1970: TimeInterval(unixTime))
        
        let isSameDay = calendar.isDate(date, inSameDayAs: date2)
        
        if !isSameDay {
            let currentDay = calendar.component(.day, from: date)
            let day = calendar.component(.day, from: date2)
            
            var dateString = ""
            let formatter = DateFormatter()
            
            if currentDay - day == 1 {
                formatter.dateFormat = "HH:mm"
                dateString = formatter.string(from: date2)
                return "\(dateString) hôm qua"
            }
            
            formatter.dateFormat = "HH:mm dd/MM/yyyy"
            dateString = formatter.string(from: date2)
            
            return dateString
        }
        
        let currentHour = calendar.component(.hour, from: date)
        let currentMinute = calendar.component(.minute, from: date)
        
        let hour = calendar.component(.hour, from: date2)
        let minute = calendar.component(.minute, from: date2)
        
        if hour == currentHour {
            if currentMinute - minute < 1 {
                return "vài giây trước"
            }
            return "\(currentMinute - minute) phút trước"
        }
        return "\(currentHour - hour) giờ trước"
        
    }
    
    func setPublishTime(_ time: Int) {
        let timeString = publishTime(from: time)
        publishTimeLabel.text = timeString
    }
    
    func setClockImage() {
        clockImageView.image = UIImage(named: "clock_normal")
    }
    
}

protocol ArticleWithComments  {
    
    var commentsCountLabel: UILabel! { get set }
    var commentImageView: UIImageView! { get set }
    
    func setCommentsCount(_ count: Int)
    func setCommentImage()
    
}

extension ArticleWithComments {
    
    func setCommentsCount(_ count: Int) {
        commentsCountLabel.text = "\(count)"
    }
    
    func setCommentImage() {
        commentImageView.image = UIImage(named: "comments_filled")
    }
    
}

protocol ArticleWithContent {
    
    var contentTextField: UITextView! { get set }
    func setContent(_ text: String?)
    
}

extension ArticleWithContent {
    
    func setContent(_ text: String?) {
        contentTextField.attributedText = attributedString(from: text)
    }
    
    private func attributedString(from content: String?) -> NSAttributedString? {
        guard let content = htmlToString(content: content) else { return nil }
        
        do {
            let attrStr = try NSAttributedString(
                data: content.data(using: String.Encoding.unicode, allowLossyConversion: true)!,
                options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                documentAttributes: nil)
            let newAttributedString = attrStr.setFamilyFont(name: "Avenir Next", size: 17)?.resizeImages(aligment: .center)
            return newAttributedString
        } catch {
            print("Error")
            // TODO: - Handle error
        }
        return nil
    }
    
    private func htmlToString(content: String?) -> String? {
        guard let content = content else { return nil }
        do {
            let doc: Document = try SwiftSoup.parse(content)
            let els = try doc.getAllElements()
            for el in els {
                if try el.className() == "Image" {
                    try el.remove()
                }
            }
            return try doc.html()
        } catch Exception.Error(_, let message) {
            print(message)
        } catch let error {
            print(error.localizedDescription)
        }
        return nil
    }
    
}
