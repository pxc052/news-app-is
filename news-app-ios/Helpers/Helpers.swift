//
//  Helpers.swift
//  news-app-ios
//
//  Created by Chien Pham on 7/30/17.
//  Copyright © 2017 Chien Pham. All rights reserved.
//

import UIKit

func delay(_ delay: Double, closure: @escaping () -> Void) {
    DispatchQueue.main.asyncAfter(
        deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
}
