//
//  Constants.swift
//  news-app-ios
//
//  Created by Chien Pham on 7/31/17.
//  Copyright © 2017 Chien Pham. All rights reserved.
//

import Foundation

struct CellHeigh {
    static let Default = 80
    static let MenuCell = 44
    static let UserCell = 100
    static let UtilityCell = 100
    
}
